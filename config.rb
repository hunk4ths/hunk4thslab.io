###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

activate :blog do |blog|
  # This will add a prefix to all links, template references and source paths
  # blog.prefix = "blog"

  blog.permalink = "{year}/{month}/{day}/{title}/index.html"
  # Matcher for blog source files
  # blog.sources = "{year}-{month}-{day}-{title}.html"
  blog.taglink = "tags/{tag}/index.html"
  # blog.layout = "layout"
  blog.summary_separator = /(READMORE)/
  blog.summary_length = 60
  blog.year_link = "{year}/index.html"
  blog.month_link = "{year}/{month}/index.html"
  blog.day_link = "{year}/{month}/{day}/index.html"
  # blog.default_extension = ".markdown"

  blog.tag_template = "tag.html"
  blog.calendar_template = "calendar.html"

  # Enable pagination
  # blog.paginate = true
  # blog.per_page = 10
  # blog.page_link = "page/{num}"
end

activate :autoprefixer do |config|
  config.browsers = ['last 2 versions', 'Explorer >= 9']
end

# Helpers
helpers do
  def icon(icon, cssclass = "", attrs = {})
    width = attrs[:width] || 76
    height = attrs[:height] || 76
    label = attrs[:label] || ""
    content_tag :svg, viewbox: "0 0 76 76", width: width, height: height, class: cssclass, aria: {label: label}, role: "img" do
      partial "includes/icons/#{icon}.svg"
    end
  end

  def logo(cssclass = "", attrs = {})
    width = attrs[:width] || 76
    height = attrs[:height] || 76
    label = attrs[:label] || ""
    content_tag :svg, viewbox: "0 0 210 194", width: width, height: height, class: cssclass, aria: {label: label}, role: "img" do
      partial "includes/logos/logo.svg"
    end
  end

  def wordmark(cssclass = "", attrs = {})
    width = attrs[:width] || 588
    height = attrs[:height] || 161
    label = attrs[:label] || ""
    content_tag :svg, viewbox: "0 0 588 237", width: width, height: height, class: cssclass, aria: {label: label}, role: "img" do
      partial "includes/logos/wordmark.svg"
    end
  end

  def footer_link(text, link)
    link_to(text, "#{data.site.about_url}#{link}")
  end
end

# Build-specific configuration
configure :development do
  activate :livereload
end

configure :build do
  set :build_dir, "public"

  activate :asset_hash
  activate :minify_css
  activate :minify_javascript
  activate :minify_html
end

ignore "/includes/*"
