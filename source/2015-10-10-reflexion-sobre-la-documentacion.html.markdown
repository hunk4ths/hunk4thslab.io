---
title: Reflexión sobre la documentación
header_subtitle: anotaciones simples sobre su generación
header_image: /images/2015-10-10.jpeg
date: 2015-10-10 03:07 UTC
tags: general
---

En todos los lugares en los que he estado, una de las dificultades recurrentes ha sido sobre la documentación, en regla general siempre inexistente o si
he tenido suerte actualizada o sin mucha información.

El factor inicial es el tiempo, en el negocio del desarrollo de software el tiempo para describir los requerimientos, la instalación, funcionamiento, mantenimiento y explicación al usuario final es crítica, por lo tanto la documentación se ignora en pro de terminar el proyecto lo antes posible.

Existen herramientas que documentan el codigo a nivel de automatización (doxygen en php, yardoc para ruby o sphinx para python) leyendo el proyecto y generando la información en base de los comenterios en el codigo, pero he aqui una cuestion :

> La documentación no solo se trata de un puñado de comentarios en el código 

También es sobre como empezar desde un estado en blanco, de que esta hecho y que harán con él sistema

En el ultimo proyecto en el que participe (a esta fecha) hubo un tiempo extra para construir la documentacion, fue un proyecto para python (django) y la documentación fue hecha en ruby con middleman

A criterio personal la documentacion la oriente hacia 3 puntos de vista distintos:

* Infraestructura
* Desarrollo/QA
* Usuario

Empiezaré describiendo las partes para que se entienda el porque de la división

### Infraestructura 
Orientado para los Operadores/SysAdmins , basicamente que requiere el software para correr desde un OS en blanco hasta el producto producción pasando por sus entornos respectivos (dev, pre,etc) , esta apoyado en ellos mismos para que les sirva de recordatorio si es que requieren volver a montar el proyecto, sea para escalarlo o simplemente para levantar entornos, aquí la información es tan solo para tenerlo operativo.

### Desarrollo/QA
Describe el proyecto a nivel de código, pueden ser funciones o funcionalidades, el asunto es que los que esten involucrados en como deberia funcionar y de que esta hecho fuera de los requerimientos del software, puedan acceder a esta información rapidamente.

### Usuario
Describe el funcionamiento fuera del software, para los clientes/consumidores del software, en caso personal como lo que yo desarrolle fue un API describia como usar dicha API, desde sus métodos hasta sus opciones y parametros (incluido ejemplos).
También inclui una subseccion de respuesta de errores. util para la depuración

Si habría que hacer un checklist de documentación de ese proyecto este podria ser:

* Secciones orientadas a la infraestructura, el desarrollo/testings, al usuario/errores
* Ejemplos en cada sección (valores por defecto, how-to , etc)
* Apartado sobre a **quien** consultarle si existen dudas
* Y lo más importante! **RETROALIMENTATE** con tu equipo, pregunta y pregunta mucho!

Extra:

1. Una de las plantillas que use para armar la documentación se llama [Web doc](http://www.frittt.com/free-documentation-html-template-docweb/) y esta en bootstrap.
2. <del>Estoy haciendo un proyecto para documentar usando middleman (extension del proyecto) se llama [docstorm](https://github.com/hunk4ths/middleman-docstorm/)</del> RDoc es un proyecto que cubre bastante bien estas necesidades, pero no olvidar que la técnica es importante también, para eso las lineas de arriba ;)


