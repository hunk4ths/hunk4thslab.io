---
title: Las fuerzas del software
date: 2016-01-13 03:45 UTC
tags: filosofia
header_subtitle: y la naturaleza que la envuelve
header_image: /images/2016-01-12.jpg
---
La inercia del trabajo , el dia a dia, a veces impide reflexionar un poco sobre el proceso en vez del resultado, obviamente lo mas importante en el software es el resultado mas alla de la metodologia o el proceso, **es que, los clientes no te pagan por hacer algo bajo cierta metodologia y tampoco les preocupa que haces primero o como lo haces** , ellos te pagan por tener el *producto* y no cualquiera, si no, el mejor *producto*, en el menor tiempo posible, **excelente es mas que bueno, hoy mejor que mañana**

La mayoria de veces lo mencionado sobre esto es irreal, todo software aparecerá más tarde de lo originalmente planeado, y con menos características, es normal y natural en el 99.9% de los escenarios, esto se muestra más claramente sobre software existente, al cual hay que hacerle mejoras... en esta caso se denotan mas como pelean dos fuerzas naturales opuestas con la misma intensidad, la **estabilidad** y la **evolución**

Basicamente la **estabilidad** permite que tu negocio siga funcionando, que puedas escalar siendo transparente para el equipo de desarrollo, evitando así que el código sea modificado para que funcione mejor en distintos escenarios, también asegurando el funcionamiento de manera optima, por el lado contrario la **evolución** viene en sentido contrario a la **estabilidad**, la **evolución** obliga a hacer cambios, si hablamos propiamente del código, vienen desde requisitos, hasta performance cambiando lo que existe por algo nuevo, corriendo hasta el mas minimo riesgo (tomando en cuenta las pruebas funcionales, unitarias, CI, QA, etc...) de que algo no planeado pueda aparecer.

Estas dos fuerzas, siempre estan actuando, siempre una dependiente a la otra, el tiempo de vida de tu software y su utilidad dependera de a cual fuerza le dejes dominar en un tiempo determinado, puesto que nunca es buena idea dejar que una fuerza siempre supere a la otra, la *decisión* es importante para cuando estas estén en conflicto, por ejemplo, si le dejas abierto siempre la evolucion, tu software dificilmente tendra un RC, o puede que tenga errores continuamente, o sea mas dificil mantenerlo mientras mas tiempo, esto se denota mas cuando los requisitos son tomados con tal urgencia que la **evolución** se torna cada vez mas relevante.

Por otro lado si dejas a la **estabilidad** como fuerza primordial , el codigo se vuelve lento en actualizaciones y procesos, en un mundo como el de TI que el tiempo pasa mas rapido, esto implica que tu negocio se puede ir muy rapido a la desaparación, debido a la competencia.
Recuerda que la **estabilidad** tambien puede ir a favor del *desarrollo* y en contra de los *requisitos*, ningun caso es una excepcion de las otras.

A veces es bueno detenerse a pensar un poco en el proceso tan solo para poder tomar una decision correcta a largo plazo...

Unos puntos a tener en cuenta sobre cosas que influyen en estas fuerzas (como auto inspeccion para definir que fuerza se le debe prestar atención):

- Si cada vez toma mas tiempo modificar algo/agregar una nueva característica.
- Si la burocracia predomina sobre todo.
- Si el equipo de desarrollo no tiene claro cual es la meta a corto o largo plazo.
- Si todo debe ser planeado (a veces el riesgo amerita muchas cosas).
- Si los errores son dificiles de corregir u ocurren muy a menudo.
- Si tienes mas gente planeando que ejecutando (una ejecucion mala y rapida es mejor que ninguna ejecución).
- Si más son los que proponen que los que hacen.

Puede que existan mas puntos, pero con este listado, uno debe detenerse a meditar y pensar un poco mas de como mejorar los procesos para tener mejores resultados, eligiendo a que fuerza dejaras pasar, puesto que la presencia de estos problemas indican la falta de equilibrio en estas.

Recuerda , a veces esto  _**no es una democracia&trade;**_